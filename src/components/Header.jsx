import React from 'react';
import PropTypes from 'prop-types';
import ViewToggler from './ViewToggler';

const Header = ({ appName, logoPath, handleClick }) => (
  <header>
    <div className="block-group">
      <div className="block-logo">
        <img src={logoPath} alt="InstaCat logo" />
        <h1>{ appName }</h1>
      </div>
      <ViewToggler handleClick={handleClick} />
    </div>
  </header>
);

Header.propTypes = {
  appName: PropTypes.string.isRequired,
  logoPath: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default Header;
