import React from 'react';
import PropTypes from 'prop-types';

const ViewToggler = ({ handleClick }) => (
  <div className="view-toggler">
    <button value="card-view" className="view-toggler-button" onClick={() => handleClick('card-view')}>
      <i className="fas fa-th-large" />
    </button>
    <button value="list" className="view-toggler-button" onClick={() => handleClick('list')}>
      <i className="fas fa-bars" />
    </button>
  </div>
);

ViewToggler.propTypes = {
  handleClick: PropTypes.func.isRequired,
};

export default ViewToggler;
