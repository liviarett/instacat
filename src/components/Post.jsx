import React from 'react';
import PropTypes from 'prop-types';
import LikeCounter from './LikeCounter';
import PostHeader from './PostHeader';
import Image from './Image';

const Post = ({ postContent }) => (
  <div className="post-container">
    <PostHeader username={postContent.username} userIconLink={`./avatars/${postContent.userIconLink}`} />
    <Image imagePath={`./images/${postContent.imageLink}`} />
    <div>
      <LikeCounter likes={postContent.likes} liked={postContent.liked} />
      <div className="caption">{postContent.description}</div>
    </div>
  </div>
);

Post.propTypes = {
  postContent: PropTypes.shape({
    username: PropTypes.string,
    userIconLink: PropTypes.string,
    imageLink: PropTypes.string,
    description: PropTypes.string,
    likes: PropTypes.number,
    liked: PropTypes.bool,
  }).isRequired,
};

export default Post;
