import React from 'react';
import PropTypes from 'prop-types';

function PostHeader({ username, userIconLink }) {
  const imageStyle = {
    backgroundImage: `url(${userIconLink})`,
    backgroundSize: 'cover',
  };

  return (
    <div className="post-header">
      <div className="user-image-wrapper" style={imageStyle} />
      <span className="username">{username}</span>
    </div>
  );
}


PostHeader.propTypes = {
  username: PropTypes.string.isRequired,
  userIconLink: PropTypes.string.isRequired,
};

export default PostHeader;
