import axios from 'axios';
import { AppContainer } from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';
import Post from './Post';

// import Routes from './Routes';

import '../less/imports.less';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feed: [],
      view: '',
    };
    this.toggleView = this.toggleView.bind(this);
  }

  componentDidMount() {
    axios.get('./photoCardsData.json').then((data) => {
      const component = this;
      component.setState({
        feed: data.data,
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  toggleView(e) {
    console.log(e);
    this.setState({
      view: e,
    });
  }

  render() {
    return (
      <AppContainer>
        <div>
          <Header appName="InstaCat" logoPath="/img/cat2.svg" handleClick={this.toggleView} />
          <section className={`main-container ${this.state.view}`}>
            {this.state.feed.map(e => (
              <Post key={e.username} postContent={e} />
             ))}
          </section>
        </div>
      </AppContainer>
    );
  }
}

if (typeof window !== 'undefined') {
  ReactDOM.render(
    (
      <App />
    ), document.getElementById('app'),
  );
}
