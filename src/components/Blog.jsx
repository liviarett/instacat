import React from 'react';
import axios from 'axios';

class Blog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      list: [],
    };
  }
  componentDidMount() {
    axios.get('./photoCardsData.json').then((data) => {
      const component = this;
      component.setState({
        list: data.data,
        loading: false,
      });
    }).catch((error) => {
      console.log(error);
    });
  }
  render() {
    return (
      <div className="block-group">
        <div className="block block-content">
          <h1>Example http request with Axios:</h1>
          {this.state.list.map(e => (
            <div key={e.username}>
              <h2>{e.userIconLink}</h2>
              <div>{e.description}</div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Blog;
