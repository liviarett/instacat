import React from 'react';
import PropTypes from 'prop-types';

class LikeCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: this.props.likes,
      liked: this.props.liked,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      likes: this.state.liked ? this.state.likes - 1 : this.state.likes + 1,
      liked: !this.state.liked,
    });
  }

  render() {
    return (
      <div className="like-counter-wrapper">
        <button className={`like-button ${this.state.liked ? 'liked' : ''}`} onClick={this.handleClick}><i className="fas fa-heart" /></button>
        <span className="likes">{this.state.likes} Likes</span>
      </div>
    );
  }
}

LikeCounter.propTypes = {
  likes: PropTypes.number.isRequired,
  liked: PropTypes.bool.isRequired,
};

export default LikeCounter;
