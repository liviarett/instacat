import React from 'react';

const About = () => (
  <div className="block-group">
    <div className="block block-content">
      <h1>About this package:</h1>
      <p>This package contains a default boilerplate project set up with some nice dev tools.</p>
    </div>
  </div>
);

export default About;
