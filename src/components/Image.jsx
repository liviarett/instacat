import React from 'react';
import PropTypes from 'prop-types';

const Image = ({ imagePath }) => (
  <div className="image-wrapper">
    <img src={imagePath} alt={imagePath} />
  </div>
);

Image.propTypes = {
  imagePath: PropTypes.string.isRequired,
};

export default Image;
